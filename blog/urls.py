from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post-list'),
    url(r'^feedback/$', views.feedback, name='feedback'),
    url(r'^posts/(?P<slug>[\w-]+)/$', views.post_detail, name='post-detail'),
    url(r'^page/(?P<page>[\d]+)/$', views.post_list, name='page'),
    url(r'^tag/(?P<tag>[\w-]+)/$', views.post_list, name='tag'),
    url(r'^about/$', views.about, name='about'),
]
