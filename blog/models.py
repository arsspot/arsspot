from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.text import slugify
from unidecode import unidecode
from django.utils import timezone
from markdown2 import markdown
from django.utils.safestring import mark_safe


def upload_location(instance, filename):
    return 'blog/{}/{}'.format(instance.slug, filename)


def make_title(text):
    _list = text.split()
    res = ' '.join([word.capitalize() for word in _list])
    return res


class Tag(models.Model):
    name = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    title = models.CharField(max_length=120, unique=True)
    sub_title = models.CharField(max_length=300, blank=True)
    slug = models.SlugField(unique=True, blank=True)
    tag = models.ManyToManyField(Tag, blank=True, related_name='related_posts')
    content = models.TextField()
    draft = models.BooleanField(default=False)
    timestamp = models.DateTimeField(default=timezone.now)
    view_counter = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        self.title = make_title(self.title)
        if not self.slug:
            self.slug = slugify(unidecode(self.title))
        super().save(*args, **kwargs)

    def content_html(self):
        content = self.content
        return mark_safe(markdown(content, extras=['fenced-code-blocks', 'tables',
                                                   'wiki-tables']))

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog:post-detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-draft", "-timestamp"]



class Feedback(models.Model):
    email = models.EmailField()
    message = models.TextField()
    replied = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['replied', 'timestamp']

    def __str__(self):
        return self.email
