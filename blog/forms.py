from django import forms
from .models import Feedback
from django.forms import ModelForm


class FeedbackForm(ModelForm):

    class Meta:
        model = Feedback
        exclude = ('replied',)
        widgets = {
            'message': forms.Textarea(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'message': 'Сообщение:',
            'email': 'Ваш e-mail:'
        }
