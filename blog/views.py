from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage
from .models import Post
from .forms import FeedbackForm
from django.shortcuts import redirect
from django.contrib import messages
from django.http import Http404


def post_list(request, page=1, tag=None):
    queryset = Post.objects.all()
    if not request.user.is_staff or not request.user.is_superuser:
        queryset = queryset.filter(draft=False)
    if tag:
        queryset = queryset.filter(tag__name=tag)
    paginator = Paginator(queryset, 10)
    try:
        posts = paginator.page(page)
    except EmptyPage:
        raise Http404
    return render(request, 'blog/post_list.html', {'posts': posts})


def post_detail(request, slug):
    queryset = Post.objects.all()
    if not request.user.is_staff or not request.user.is_superuser:
        queryset = queryset.filter(draft=False)
    post = get_object_or_404(queryset, slug=slug)
    if post:
        post.view_counter += 1
        post.save()
    return render(request, 'blog/post_detail.html', {'post': post})


def feedback(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Ваше сообщение успешно доставлено!')
            return redirect('blog:feedback')
    else:
        form = FeedbackForm()
    return render(request, 'blog/feedback.html', {'form': form})


def about(request):
    return render(request, 'blog/about.html')
