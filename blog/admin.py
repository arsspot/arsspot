from django.contrib import admin
from django.db import models
from .models import Post, Tag, Feedback
from pagedown.widgets import AdminPagedownWidget


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'timestamp', 'view_counter', 'draft')
    prepopulated_fields = {"slug": ("title",)}
    formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget(show_preview=True)},
    }


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('email', 'timestamp', 'replied', )
    readonly_fields = ('timestamp',)

admin.site.register(Post, PostAdmin)
admin.site.register(Tag)
admin.site.register(Feedback, FeedbackAdmin)
