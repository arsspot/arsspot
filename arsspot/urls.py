from django.conf.urls import url, include
from django.contrib import admin
from home import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^blog/', include('blog.urls', namespace='blog'), name='blog_app'),
    url(r'^$', views.home, name='home'),
]

admin.site.site_header = "Блог"
